package Dao;

import com.sun.org.apache.xml.internal.security.signature.ObjectContainer;
import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.scene.chart.PieChart;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by mohit on 3/28/18.
 */
//@RunWith(Parameterized.class)
public class UsersDaoTest extends TestCase {
    public UsersDao usersDao;
    public List<DataAccessObject> list;

    @Before
    public void setUp() throws Exception {
        usersDao = new UsersDao();
        list = new ArrayList<DataAccessObject>();
    }

    @Test
    public void testDAO() {
//        usersDao = new UsersDao();
        list = usersDao.getAllUsers();
        for (DataAccessObject dataAccessObject : list) {
            System.out.println(dataAccessObject.getName() + " " + dataAccessObject.getCity() + " " + dataAccessObject.getPhone());
        }
    }
}
