package dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by mohit on 3/21/18.
 */
public class DBConnection {
    public static Connection createConnection(){
        Connection con = null;

        String url="jdbc:mysql://localhost/userdb";
        String username = "root";
        String password ="";

        try{
            try{
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e){
                System.out.println("NO CONNECTION");
                e.printStackTrace();
            }

            con = DriverManager.getConnection(url, username, password);
            System.out.println("Post establishing a DB connection -" + con);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return con;
    }
}
