package Dao;

import dbConnection.DBConnection;
import javafx.scene.chart.PieChart;
import javafx.scene.input.TouchPoint;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by mohit on 3/23/18.
 */
public class UsersDao {
    public Connection connection;
    public UsersDao(){
        connection = DBConnection.createConnection();
    }
    public List<DataAccessObject> getAllUsers(){
        List<DataAccessObject> users =  new ArrayList();
        try{
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT a.*, roleDescription FROM tbl_roles as x,tbl_users as a,tbl_userRoles as b WHERE a.userId = b.userId AND b.roleId = x.roleId ORDER BY a.userId ASC");
            while(resultSet.next()){
                DataAccessObject dataAccessObject = new DataAccessObject();
                dataAccessObject.setName(resultSet.getString("name"));
                dataAccessObject.setEmail(resultSet.getString("email"));
                dataAccessObject.setCity(resultSet.getString("city"));
                dataAccessObject.setPhone(resultSet.getInt("phone"));
                dataAccessObject.setRoleDescription(resultSet.getString("roleDescription"));
                dataAccessObject.setUserId(resultSet.getInt("userId"));
                users.add(dataAccessObject);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return users;
    }

    public void addUser(DataAccessObject data){
        try{
            String sql2="INSERT INTO tbl_userRoles (userId, roleId) VALUES ('"+data.getUserId()+"', 2);";
            Statement statement= connection.createStatement();
            statement.executeUpdate("INSERT INTO tbl_users (userId,username,email, password,name,city,phone)\n" +
                    "VALUES('"+data.getUserId()+"','"+data.getUserName()+"','"+data.getEmail()+"','"+data.getPassword()+"','"+data.getName()+"','"+data.getCity()+"','"+data.getPhone()+"');");
            System.out.println("USERIDDDDDDD"+data.getUserId());
            statement.executeUpdate(sql2);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public void addUserByAdmin(DataAccessObject data){
        try{
            String sql1 ="INSERT INTO tbl_users (userId,username,email, password,name,city,phone)\n" +
                    "VALUES('"+data.getUserId()+"','"+data.getUserName()+"','"+data.getEmail()+"','"+data.getPassword()+"','"+data.getName()+"','"+data.getCity()+"','"+data.getPhone()+"');";
            String sql2="INSERT INTO tbl_userRoles (userId, roleId) VALUES ('"+data.getUserId()+"','"+data.getRoleId()+"');";
            Statement statement=connection.createStatement();
            statement.executeUpdate(sql1);
            statement.executeUpdate(sql2);

        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public void updateUser(int userId, int roleId){
        try{
            Statement statement = connection.createStatement();
            statement.executeUpdate("UPDATE tbl_userRoles SET roleId="+roleId+" WHERE userId="+userId);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public DataAccessObject userProfileByID(String userName){
        DataAccessObject user = new DataAccessObject();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet= statement.executeQuery("SELECT name,city,phone from tbl_users WHERE userName="+"'"+userName+"'");
            if(resultSet.next()){
                user.setName(resultSet.getString("name"));
                user.setCity(resultSet.getString("city"));
                user.setPhone(resultSet.getInt("phone"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return user;
    }
    public void deleteUser(int userId){
        try{
            String sql1="DELETE FROM tbl_userRoles WHERE tbl_userRoles.userId ="+userId;
            String sql2="DELETE FROM tbl_users WHERE tbl_users.userId ="+userId;
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql1);
            statement.executeUpdate(sql2);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
