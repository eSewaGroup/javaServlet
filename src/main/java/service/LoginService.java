package service;

import Dao.DataAccessObject;
import dbConnection.DBConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by mohit on 3/21/18.
 */
public class LoginService {
    public String authenticate(DataAccessObject dataAccessObject){
//        if(password == null || password.trim() == "" ){
//            return false;
//        }
//        return true;
        String username = dataAccessObject.getUserName();
        String password = dataAccessObject.getPassword();

        Connection con = null;
        Statement statement = null;
        ResultSet resultSet = null;

        String userNameDB ="";
        String passwordDB = "";
        String roleDB = "";

        try{
            con = DBConnection.createConnection();
            statement = con.createStatement();
            resultSet =  statement.executeQuery("SELECT a.userName,password,roleDescription FROM tbl_roles as x,tbl_users as a,tbl_userRoles as b WHERE a.userId = b.userId AND b.roleId = x.roleId");

            while (resultSet.next()){
                userNameDB = resultSet.getString("userName");
                passwordDB = resultSet.getString("password");
                roleDB = resultSet.getString("roleDescription");
                if(username.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("admin"))
                    return "admin";
                else if(username.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("user"))
                    return "user";
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

       return "Invalid";
    }
}
