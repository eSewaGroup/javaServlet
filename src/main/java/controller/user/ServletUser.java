package controller.user;

import Dao.DataAccessObject;
import Dao.UsersDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by mohit on 3/22/18.
 */
@WebServlet("/user/userAccount")
public class ServletUser extends HttpServlet {
    private UsersDao usersDao;

    public ServletUser() {
        usersDao = new UsersDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session  = request.getSession();
        String userName = (String)session.getAttribute("userName");
        DataAccessObject user =  usersDao.userProfileByID(userName);
        request.setAttribute("user",user);
        RequestDispatcher dispatcher = request.getRequestDispatcher("user.jsp");
        dispatcher.forward(request, response);
    }
}
