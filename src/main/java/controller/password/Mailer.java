package controller.password;

import com.mysql.jdbc.interceptors.SessionAssociationInterceptor;
import dbConnection.DBConnection;

import java.sql.*;
import java.util.Properties;
import java.util.*;
import javax.mail.Session;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.Transport;
import javax.activation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by mohit on 4/2/18.
 */
public class Mailer {
    private RandomString rd = new RandomString();
    private Connection conn = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    private ResultSet rs = null;

    public void send(String emailaddress, HttpServletRequest request) throws SQLException, ClassNotFoundException {
        int userId = 0;
        String from = "paltalkmajor@gmail.com";
        final String username = "paltalkmajor@gmail.com";
        final String password = "mohit123";
        String email = new String();
        String userName = new String();
        String token = new String();
        String url = new String();
        HttpSession s = request.getSession();
        String query = "SELECT userId,userName,email,password FROM tbl_users WHERE email=" + "'" + emailaddress + "'";

        conn = DBConnection.createConnection();
        ps = conn.prepareStatement("INSERT INTO tbl_userToken values (?,?,?,?)");
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        token = rd.randomString(10);
        while (rs.next()) {
            userId = rs.getInt("userId");
            userName = rs.getString("userName");
            email = rs.getString("email");
        }
        s.setAttribute("userId", userId);
        ps.setInt(1, userId);
        ps.setString(2,userName);
        ps.setString(3, token);
        ps.setInt(4,0);
        ps.executeUpdate();
        url = "<h2>Click the link below to change your password!</h2><br><a href = " + "'" + "http://localhost:8080/passwordResetForm.jsp?token=" + token +"&userName="+ userName+"'"+">Click Here!</a>";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.debug", "true");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Password Recover");
            message.setContent(url, "text/html");

            Transport.send(message);

            System.out.println("Email Sent Successfully");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } finally {
            conn.close();
        }
    }

}
