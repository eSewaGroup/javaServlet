package controller.password;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by mohit on 4/2/18.
 */
@WebServlet("/PasswordReset")
public class PasswordReset extends HttpServlet {

    private Mailer mailer;
    @Override
    public void init(){
        String hostname="smtp.gmail.com";
        int port = 587;
        String username ="paltalkmajor@gmail.com";
        String password ="mohit123";
        mailer = new Mailer();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String emailaddress = request.getParameter("emailadd");
            // Do some validations and then send mail:

            String msg = "";
            try {
                msg = "Mail Successfully Sent!!";
                System.out.println("Check");

                mailer.send(emailaddress, request);
                System.out.println("Check2");

               request.setAttribute("message", "Mail succesfully sent!");
                request.getRequestDispatcher("/resetPwdMessages.jsp").forward(request, response);
            } catch (Exception e) {
                request.setAttribute("message", "Invalid email-Address!");
                request.getRequestDispatcher("/resetPwdMessages.jsp").forward(request, response);
            }
        } finally {
            out.close();
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
