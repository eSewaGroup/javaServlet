package controller.password;

import dbConnection.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.tlv.PermittedTaglibsTLV;
import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by mohit on 4/3/18.
 */
@WebServlet("/resetServlet")
public class resetServlet extends HttpServlet {
    private Statement stmt = null;
    private Connection conn = null;
    private ResultSet rs = null;
    int userIdDB, statusDB;
    String tokenDB= new String();
    String userNameDB = new String();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token= (String) request.getParameter("token");
        String userName=(String)request.getParameter("userName");
//        System.out.println("parameter ko name:"+userName);
//        System.out.println("parameter ko token:"+token);
//        PrintWriter writer = response.getWriter();
//        writer.print(token + "   "+userName);
        String query = "SELECT * FROM tbl_userToken WHERE userName="+"'"+userName+"'"+" AND token="+"'"+token+"'";
        conn = DBConnection.createConnection();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                //userIdDB = rs.getInt("userId");
                tokenDB = rs.getString("token");
                statusDB = rs.getInt("status");
                userNameDB = rs.getString("userName");
            }
            if(userNameDB.equalsIgnoreCase(userName) && tokenDB.equalsIgnoreCase(token) && statusDB == 0 ){
                String password = request.getParameter("password");
                stmt.executeUpdate("UPDATE tbl_users SET password="+"'"+password+"'"+" WHERE userName="+"'"+userName+"'");
                stmt.executeUpdate("UPDATE tbl_userToken SET status=1 WHERE userName="+"'"+userName+"'"+" AND token="+"'"+token+"'");
                request.setAttribute("message", "Your password has been changed.");
                request.getRequestDispatcher("/resetPwdMessages.jsp").forward(request, response);
            }
            else{
                request.setAttribute("message", "The link cannot be used anymore!");
                request.getRequestDispatcher("/resetPwdMessages.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
