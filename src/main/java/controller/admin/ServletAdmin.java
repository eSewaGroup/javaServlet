package controller.admin;

import Dao.DataAccessObject;
import Dao.UsersDao;

import javax.jws.soap.SOAPBinding;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;

/**
 * Created by mohit on 3/22/18.
 */
@WebServlet("/admin/adminAccount")
public class ServletAdmin extends HttpServlet {
    private UsersDao usersDao;

    public ServletAdmin() {
        usersDao = new UsersDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         String action = request.getParameter("action");
        try {
            if(action == null){
                listUser(request, response);
            }
            else if (action.equalsIgnoreCase("add")) {
                insertUser(request, response);
            }
            else if(action.equalsIgnoreCase("new")){
                showNewForm(request, response);
            }
            else if(action.equalsIgnoreCase("delete")){
                deleteUser(request, response);
            }
            else if(action.equalsIgnoreCase("updateForm")){
                showUpdateForm(request, response);
            }
            else if(action.equalsIgnoreCase("update")){
                updateUserRole(request, response);
            }
            else
                listUser(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
}
    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("insert.jsp");
        dispatcher.forward(request, response);
    }
    private void listUser(HttpServletRequest request, HttpServletResponse response) throws IOException,SQLException,ServletException{
        List<DataAccessObject> users = usersDao.getAllUsers();
        request.setAttribute("users",users);
        RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
        dispatcher.forward(request, response);
    }
    private void insertUser(HttpServletRequest request, HttpServletResponse response)throws IOException,SQLException{
        DataAccessObject data= new DataAccessObject();
        data.setUserId(Integer.parseInt(request.getParameter("userId")));
        data.setUserName(request.getParameter("userName"));
        data.setEmail(request.getParameter("email"));
        data.setPassword(request.getParameter("password"));
        data.setName(request.getParameter("name"));
        data.setCity(request.getParameter("city"));
        data.setPhone(Integer.parseInt(request.getParameter("phone")));
        data.setRoleId(Integer.parseInt(request.getParameter("roleId")));
        usersDao.addUserByAdmin(data);
        response.sendRedirect("adminAccount?action=list");
    }
    public void deleteUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        int id = Integer.parseInt(request.getParameter("userId"));
        usersDao.deleteUser(id);
        response.sendRedirect("adminAccount?action=list");
    }
    public void showUpdateForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException{
        int id= Integer.parseInt(request.getParameter("userId"));
        request.setAttribute("userId", id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("update.jsp");
        dispatcher.forward(request, response);
    }
    public void updateUserRole(HttpServletRequest request, HttpServletResponse response) throws SQLException,IOException{
        int userId= Integer.parseInt(request.getParameter("userId"));
        int roleId = Integer.parseInt(request.getParameter("roleId"));
        usersDao.updateUser(userId, roleId);
        response.sendRedirect("adminAccount?action=list");
    }
}
//    UsersDao usersDao = new UsersDao();
//    DataAccessObject data= new DataAccessObject();
//        data.setUserId(Integer.parseInt(request.getParameter("userId")));
//                data.setUserName(request.getParameter("userName"));
//                data.setPassword(request.getParameter("password"));
//                data.setName(request.getParameter("name"));
//                data.setCity(request.getParameter("city"));
//                data.setPhone(Integer.parseInt(request.getParameter("phone")));
//                usersDao.addUserByAdmin(data);