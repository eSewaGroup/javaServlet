package controller.login;

import Dao.DataAccessObject;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by mohit on 3/21/18.
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String userName, password;
        userName = request.getParameter("userName");
        password = request.getParameter("password");

        LoginService loginService = new LoginService();

        DataAccessObject dataAccessObject = new DataAccessObject();
        dataAccessObject.setUserName(userName);
        dataAccessObject.setPassword(password);
        String result = loginService.authenticate(dataAccessObject);
        if(result.equals("admin")){
            HttpSession session = request.getSession();
            session.setAttribute("userName", userName);
            session.setAttribute("userRole", result);
            response.sendRedirect("admin/adminAccount?action=list");
            return;
        }
        else if(result.equals("user")){
            HttpSession session = request.getSession();
            session.setAttribute("userName", userName);
            session.setAttribute("userRole", result);
            response.sendRedirect("user/userAccount");
            return;
        }
        else{
            request.setAttribute("message", "Invalid ");
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action == null){
            request.getRequestDispatcher("index.jsp").forward(request,response);
        }
        else if(action.equalsIgnoreCase("logout")){
            HttpSession session = request.getSession();
//            session.removeAttribute("userName");
            session.invalidate();
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }
}
