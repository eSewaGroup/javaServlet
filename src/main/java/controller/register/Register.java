package controller.register;

import Dao.DataAccessObject;
import Dao.UsersDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by mohit on 3/29/18.
 */
@WebServlet("/register")
public class Register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsersDao usersDao = new UsersDao();
        DataAccessObject data= new DataAccessObject();
        data.setUserId(Integer.parseInt(request.getParameter("userId")));
        data.setUserName(request.getParameter("userName"));
        data.setEmail(request.getParameter("email"));
        data.setPassword(request.getParameter("password"));
        data.setName(request.getParameter("name"));
        data.setCity(request.getParameter("city"));
        data.setPhone(Integer.parseInt(request.getParameter("phone")));
        usersDao.addUser(data);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
