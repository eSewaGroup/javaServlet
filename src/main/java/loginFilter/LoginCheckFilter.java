package loginFilter;

import Dao.DataAccessObject;
import service.LoginService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;
import java.awt.geom.RectangularShape;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by mohit on 3/21/18.
 */
@WebFilter("/loginFilter")
public class LoginCheckFilter implements Filter {
    public void init(FilterConfig arg0) throws ServletException {
    }

    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();
        Boolean adminURI = request.getRequestURI().endsWith("admin/adminAccount");
        Boolean userURI = request.getRequestURI().endsWith("user/userAccount");
        Boolean loginURI = request.getRequestURI().endsWith("login");
        System.out.println(session.getAttribute("userName"));
//        if(request.getRequestURI().endsWith("/login")){
//            chain.doFilter(req,resp);
//        }
//        else if(session.getAttribute("userName") != null && session.getAttribute("userRole").equals("user") && adminURI){
//            request.getRequestDispatcher("index.jsp").forward(req,resp);
//       }
//       else if(session.getAttribute("userName")== null){
//            request.getRequestDispatcher("index.jsp").forward(req,resp);
//        }
        if (session.getAttribute("userName") == null && !(loginURI)) {
            //request.getRequestDispatcher("/login").forward(req, resp);
            response.sendRedirect("/login");
        } else if (session.getAttribute("userName") != null && session.getAttribute("userRole").equals("user") && adminURI) {
            request.getRequestDispatcher("/user/userAccount").forward(req, resp);
        } else {
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setDateHeader("Expires", 0);
            chain.doFilter(req, resp);
        }
    }

    public void destroy() {
    }
}
