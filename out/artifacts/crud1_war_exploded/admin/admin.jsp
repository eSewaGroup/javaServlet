<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: mohit
  Date: 3/21/18
  Time: 10:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1">
    <thead>
    <tr>
        <th>UserId</th>
        <th>Email</th>
        <th>Name</th>
        <th>City</th>
        <th>Phone Numnber</th>
        <th>User Roles</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${users}" var="user">
        <tr>
            <td><c:out value="${user.userId}"></c:out> </td>
            <td><c:out value="${user.email}"></c:out> </td>
            <td><c:out value="${user.name}"/></td>
            <td><c:out value="${user.city}"/></td>
            <td><c:out value="${user.phone}"/></td>
            <td><c:out value="${user.roleDescription}"/></td>
            <td><a href="adminAccount?action=updateForm&userId=<c:out value="${user.userId}"/>">Update</a></td>
            <td><a style="color:red" href="adminAccount?action=delete&userId=<c:out value="${user.userId}"/>">Delete</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<p><a href="adminAccount?action=new">Add User</a></p>
<p><a href="/user/userAccount">View Profile</a></p>
</br>
<p><a href="/login?action=logout">Logout</a> </p>
</body>
</html>
