<%--
  Created by IntelliJ IDEA.
  User: mohit
  Date: 3/21/18
  Time: 10:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>$Title$</title>
</head>
<body>
<fieldset>
    <legend>Login</legend>
    ${message}<br>
    <form method="post" action="login">
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td>Username</td>
                <td><input type="text" name="userName"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="Login"></td>
            </tr>
        </table>
    </form>
</fieldset>
<p><a href="/emailForResetPwd.jsp">Password Reset</a></p>
</body>
</html>

