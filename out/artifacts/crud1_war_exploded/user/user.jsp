<%@ page import="Dao.DataAccessObject" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: mohit
  Date: 3/21/18
  Time: 4:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    DataAccessObject object = (DataAccessObject)request.getAttribute("user");

%>
<h2>Welcome! <%= object.getName() %></h2>
<hr><h2>Location</h2>
<%= object.getCity()%>
<hr>
<h2>Phone</h2>
<%= object.getPhone()%>
</br>
<p><a href="/login?action=logout">Logout</a> </p>
</body>
</html>
